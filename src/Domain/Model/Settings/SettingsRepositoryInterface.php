<?php

namespace App\Domain\Model\Settings;

/**
 * Interface SettingsRepositoryInterface
 * @package App\Domain\Model\Settings
 */
interface SettingsRepositoryInterface
{

    /**
     * @param int $settingsId
     * @return Settings
     */
    public function findById(int $settingsId): ?Settings;
    
    /**
     * @return array
     */
    public function findAll(): array;
    
    /**
     * @param Settings $settings
     */
    public function save(Settings $settings): void;

    /**
     * @param Settings $settings
     */
    public function delete(Settings $settings): void;

}