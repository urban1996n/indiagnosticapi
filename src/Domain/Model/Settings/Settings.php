<?php
namespace App\Domain\Model\Settings;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Settings
 * @ORM\Entity
 * @package App\Domain\Model\Settings
 */
class Settings
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $clientName;
    
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $machineName;
    
    /**
     * @ORM\Column(type="string")
     * @var string 
     */
    private $ipAddress;
    
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $pingTest;
    
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $httpTest;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $otherTest;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var float
     */
    private $dateTime;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $pingResult;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    private $httpResult;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    private $otherResult;
    
    /**
     * @var bool
     */
    private $overallResult = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getClientName(): string
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     */
    public function setClientName(string $clientName): void
    {        
        $this->clientName = $clientName;
    }

    /**
     * @return string
     */
    public function getMachineName(): string
    {
        return $this->machineName;
    }

    /**
     * @param string $machineName
     */
    public function setMachineName(string $machineName): void
    {        
        $this->machineName = $machineName;
    }
    
    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setAddress(string $ipAddress): void
    {        
        $this->ipAddress = $ipAddress;
    }
    
    /**
     * @return int
     */
    public function getPingTest(): int
    {
        return $this->pingTest;
    }

    /**
     * @param int $pingTest
     */
    public function setPingTest(int $pingTest): void
    {        
        $this->pingTest = $pingTest;
    }
    
    /**
     * @return int
     */
    public function getHttpTest(): int
    {
        return $this->httpTest;
    }

    /**
     * @param int $httpTest
     */
    public function setHttpTest(int $httpTest): void
    {        
        $this->httpTest = $httpTest;
    }
    /**
     * @return int
     */
    public function getOtherTest(): int
    {
        return $this->otherTest;
    }

    /**
     * @param int $otherTest
     */
    public function setOtherTest(int $otherTest): void
    {        
        $this->otherTest = $otherTest;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @param array $sequenceCount
     */
    public function setDateTime(\DateTime $dateTime): void
    {        
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function getPingResult():?string
    {
        return $this->pingResult;
    }

    /**
     * @param string $pingResult
     */
    public function setPingResult(string $pingResult): void
    {        
        $this->pingResult = $pingResult;
    }
    
    /**
     * @return int
     */
    public function getHttpResult():?int
    {
        return $this->httpResult;
    }

    /**
     * @param int $httpResult
     */
    public function setHttpResult(int $httpResult): void
    {        
        $this->httpResult = $httpResult;
    }
    
    /**
     * @return int
     */
    public function getOtherResult():?int
    {
        return $this->otherResult;
    }

    /**
     * @param int $otherResult
     */
    public function setOtherResult(int $otherResult): void
    {        
        $this->otherResult = $otherResult;
    }

    /**
     * @return bool
     */
    public function getOverallResult():?bool
    {
        return $this->overallResult;
    }

    /**
     * @param bool $overallResult
     */
    public function setOverallResult(bool $overallResult): void
    {        
        $this->overallResult = $overallResult;
    }    
}