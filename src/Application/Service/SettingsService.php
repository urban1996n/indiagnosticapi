<?php

namespace App\Application\Service;

use App\Domain\Model\Settings\Settings;
use App\Domain\Model\Settings\SettingsRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class SettingsService
 * @package App\Application\Service
 */
final class SettingsService
{

    /**
     * @var SettingsRepositoryInterface
     */
    private $settingsRepository;

    /**
     * SettingsService constructor.
     * @param SettingsRepositoryInterface $settingsRepository
     */
    public function __construct(SettingsRepositoryInterface $settingsRepository){
        $this->settingsRepository = $settingsRepository;
    }

     /**
     * @param int $machineId
     * @return Settings|null
     */
    public function findById(int $machineId): ?Settings
    {
        $settings = $this->settingsRepository->findById($machineId);
        return $settings;
    }

    /**
     * @return array|null
     */
    public function getAllSettings(): ?array
    {
        $settings = $this->settingsRepository->findAll();
        return $settings;
    }

    /**
     * @param int $id
     * @param string $machineName
     * @param float $startPoint
     * @param float $movePoint
     * @param float $overloadPower
     * @return Settings
     */
    public function addSettings(string $machineName, string $clientName, string $ipAddress, int $pingTest, int $httpTest, int $otherTest): Settings
    {
        $settings = new Settings();

        $settings->setMachineName($machineName);
        $settings->setClientName($clientName);
        $settings->setAddress($ipAddress);
        $settings->setPingTest($pingTest);
        $settings->setHttpTest($httpTest);
        $settings->setOtherTest($otherTest);

        $this->settingsRepository->save($settings);

        return $settings;
    }
    
     /**
     * @param int $settingsId
     * @param string $machineName
     * @param float $startPoint
     * @param float $movePoint
     * @param float $overloadPower
     * @return Settings
     * @throws EntityNotFoundException
     */
    public function updateSettings(int $settingsId, string $machineName, string $clientName, string $ipAddress, int $pingTest, int $httpTest, int $otherTest): Settings
    {
        $settings = $this->settingsRepository->findById($settingsId);
        if (!$settings) {
            throw new EntityNotFoundException('Settings has not been found');            
        }
        
        $settings->setMachineName($machineName);
        $settings->setClientName($clientName);
        $settings->setAddress($ipAddress);
        $settings->setPingTest($pingTest);
        $settings->setHttpTest($httpTest);
        $settings->setOtherTest($otherTest);

        $this->settingsRepository->save($settings);

        return $settings;
    }

    /**
    * @param int $settingsId
    * @param \DateTime $startDate
    * @param \DateTime $endDate
    * @return Settings
    * @throws EntityNotFoundException
    */
    public function setTestsResults(int $settingsId, bool $pingResult, bool $httpResult, bool $otherResult): ?Settings
    {
        $settings = $this->settingsRepository->findById($settingsId);
        
        $settings->setPingResult($pingTest);
        $settings->setHttpResult($httpTest);
        $settings->setOtheResult($otherTest);

        $this->settingsRepository->save($settings);
        
        return $settings;   
    }

     /**
     * @param int $settingsId
     * @param string $name
     * @throws EntityNotFoundException
     */
    public function deleteSettings(int $settingsId): void
    {
        $settings = $this->settingsRepository->findById($settingsId);
        if (!$settings) {
            throw new EntityNotFoundException('Settings has not been found');            
        }
        $this->settingsRepository->delete($settings);

    }

}