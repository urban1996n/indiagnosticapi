<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Settings\Settings;
use App\Domain\Model\Settings\SettingsRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingsRepository
 * @package App\Infrastructure\Repository
 */
final class SettingsRepository implements SettingsRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * SettingsRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;  
        $this->objectRepository = $this->entityManager->getRepository(Settings::class);
    }
    
    /**
     * @return Settings
     */
    private function getAll() : ?array
    {
        $settings = $this->entityManager->createQueryBuilder()
        ->select('s')
        ->from(Settings::class, 's')
        ->orderBy('s.clientName', 'ASC')
        ->getQuery()
        ->getResult();
        
        return $settings;
    }
    
    /**
     * @param int $settingsId
     * @return Settings
     */
    public function findById(int $settingsId): ?Settings
    {
        $settings = $this->objectRepository->find($settingsId);
        
        if($settings->getPingTest() == 1 && $settings->getHttpTest() == 1 && $settings->getOtherTest() == 1)
        {
            if($settings->getOtherResult() == 1)
            {
                $settings->setOverallResult(true);
            }
        }
        elseif($settings->getPingTest() == 1 && $settings->getHttpTest() == 0 && $settings->getOtherTest() == 1)
        {
            if($settings->getPingResult() == 1 && $settings->getOtherResult() == 1)
            {
                $settings->setOverallResult(true);
            }
        }
        elseif($settings->getPingTest() == 0 && $settings->getHttpTest() == 1 && $settings->getOtherTest() == 1)
        {
            if($settings->getHttpResult() == 1 && $settings->getOtherResult() == 1)
            {
                $settings->setOverallResult(true);
            }
        }
        elseif($settings->getPingTest() == 1 && $settings->getHttpTest() == 0 && $settings->getOtherTest() == 0)
        {
            if($settings->getPingResult() || ($settings->getPingResult() && !empty($settings->getPingResult()) ))
            {
                $settings->setOverallResult(true);
            }
        }

        else
        {
            if($settings->getOtherResult() == 1)
            {
                $settings->setOverallResult(true);
            }
        }

        return $settings;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $settings = $this->getAll();
        
        foreach($settings as $setting)
        {
            if($setting->getPingTest()&& $setting->getHttpTest()&& $setting->getOtherTest())
            {
                if($setting->getOtherResult() 
                 &&($setting->getPingResult() || ($setting->getPingResult() && !empty($setting->getPingResult())))
                 && $setting->getHttpResult())
                {
                    $setting->setOverallResult(true);
                }
            }
            elseif($setting->getPingTest() && !$setting->getHttpTest() && $setting->getOtherTest())
            {
                if(($setting->getPingResult() || ($setting->getPingResult() && !empty($setting->getPingResult())))
                   && $setting->getOtherResult())
                {
                    $setting->setOverallResult(true);
                }
            }
            elseif($setting->getPingTest() && $setting->getHttpTest() && !$setting->getOtherTest())
            {
                if(($setting->getPingResult() || ($setting->getPingResult() && !empty($setting->getPingResult())))
                   && $setting->getHttpResult())
                {
                    $setting->setOverallResult(true);
                }
            }
            
            elseif(!$setting->getPingTest() && $setting->getHttpTest() && $setting->getOtherTest())
            {
                if($setting->getHttpResult() && $setting->getOtherResult())
                {
                    $setting->setOverallResult(true);
                }
            }
            elseif($setting->getPingTest())
            {
                if($setting->getPingResult() || ($setting->getPingResult() && !empty($setting->getPingResult())))
                {
                    $setting->setOverallResult(true);
                }
            }
            elseif($setting->getHttpTest())
            {
                if($setting->getHttpResult())
                {
                    $setting->setOverallResult(true);
                }
            }
            else
            {
                if($setting->getOtherResult())
                {
                    $setting->setOverallResult(true);
                }
            }
        }

        return $settings;
    }

    /**
     * @param Settings $settings
     */
    public function save(Settings $settings): void
    {
        $this->entityManager->persist($settings);
        $this->entityManager->flush();
    }
    
    /**
     * @param Settings $settings
     */
    public function delete(Settings $settings): void
    {
        $this->entityManager->remove($settings);
        $this->entityManager->flush();
    }

}