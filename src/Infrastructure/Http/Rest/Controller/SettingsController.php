<?php

namespace App\Infrastructure\Http\Rest\Controller;

use App\Application\Service\SettingsService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Clas SettingsController
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class SettingsController extends FOSRestController
{
    /**
     * @var SettingsService
     */
    private $settingsService;
    
    /**
     * SettingsController constructor.
     * @param SettingsService $settingsService
     * @param ProductService $productService
     */
    public function __construct(SettingsService $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * Creates an Settings resource
     * @Rest\Post("/settings.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function postSettings(Request $request): View
    {
        $settings = $this->settingsService->addSettings($request->get('machine_name'), $request->get('client_name'),  $request->get('ip_address'), $request->get('ping_test'), $request->get('http_test'), $request->get('other_test'));

        // In case our POST was a succes we need to return a 201 HTTP CREATED response with the created object
        return View::create($settings, Response::HTTP_CREATED);
    }

    /**
     * Retrieves an Settings resource
     * @Rest\Get("/settings/id/{settingsId}.{_format}", defaults={"_format"="json"})
     * @param int $settingsId
     * @return View
     */
    public function getSetting(int $settingsId): View
    {
        $settings = $this->settingsService->findById($settingsId);


        // In case our GET was a succes we need to return a 200 HTTP OK response with the request object
        return View::create($settings, Response::HTTP_OK);
    }

    /**
     * Retrieves a collection of Settings resource
     * @Rest\Get("/settings.{_format}", defaults={"_format"="json"})
     * @return View
     */
    public function getSettings(): View
    {
        $settings = $this->settingsService->getAllSettings();

        // In case our GET was a succes we need to return a 200 HTTP OK response with the collection of settings object
        return View::create($settings, Response::HTTP_OK);
    }

    /**
     * Replaces Settings resource
     * @Rest\Put("/settings/{settingsId}.{_format}", defaults={"_format"="json"})
     * @param int $settingsId
     * @param Request $request
     * @return View
     */
    public function putSettings(int $settingsId, Request $request): View
    {        
        $this->settingsService->updateSettings($settingsId, $request->get('machine_name'), $request->get('client_name'), $request->get('ip_address'), $request->get('ping_test'), $request->get('http_test'), $request->get('other_test'));

        // In case our PUT was a succes we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create(Response::HTTP_OK);
    }

    /**
     * Puts / Replaces Settings' sequence property
     * @Rest\Put("/settings/test/{settingsId}.{_format}", defaults={"_format"="json"})
     * @param int $settingsId
     * @param Request $request
     * @return View
     */
    public function putTests(int $settingsId, Request $request): View
    {        
        $this->settingsService->setTestsResults($settingsId, new \DateTime($request->get('datetime')), $request->get('http_result'), $request->get('pint_result'), $request->get('other_result'));

        // In case our PUT was a succes we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create(Response::HTTP_OK);
    }

    /**
     * Removes the Settings resource
     * @Rest\Delete("/settings/{settingsId}.{_format}", defaults={"_format"="json"})
     * @param int $settingsId
     * @return View
     */
    public function deleteSettings(int $settingsId): View
    {
        $this->settingsService->deleteSettings($settingsId);
        // In case our DELETE was a succes we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create(["mesage"=>"Settings has been deleted"], Response::HTTP_NO_CONTENT);
    }
}