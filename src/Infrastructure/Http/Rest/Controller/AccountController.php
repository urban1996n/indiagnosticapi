<?php

namespace App\Infrastructure\Http\Rest\Controller;

use App\Application\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\RequestStack;
/**
 * Class AccountController - made for users to manage and create their accounts(only user with ROLE_ADMIN has permission to access UserController, and manage accounts without additional authentication)
 * 
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class AccountController extends FOSRestController
{
    /**
     * @var UserService
     */
    private $userService;
    
    /**
     * @var \Symfony\Component\Serializer\Serializer;
     */
    private $serializer;

    /**
     * @var int
     */
    private $userId;

    /**
     * UserController constructor.
     * @param UserService $userService 
     * @param RequestStack $requestStack 
     */
    public function __construct(UserService $userService, RequestStack $requestStack)
    {   
        //create serializer(normalizer) instance for adjusting response content
        $this->serializer = new Serializer([new ObjectNormalizer()]);

        $this->userService = $userService;
        
        //getting request headers for authorization
        $request = $requestStack->getCurrentRequest();

        //Authorizating user account with credentials given in request , excluding account creating path from authentication
        if($request->getPathInfo()!="/api/account/new" && $request->getPathInfo()!="/api/login"){

            $user = $this->userService->authenticate($request->get('apiKey'),$request->get('username'),$request->get('password'));
            //store User ID for working with it's account with no additional parameters passed to actions
            $this->userId = $user->getId();
        }
    }

    /**
     * Creates new account
     * @Rest\Post("/account/new.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function createAccount(Request $request): View
    {
        $user = $this->userService->addUser($request->get('username'), $request->get('name'), $request->get('surname'), $request->get('password'),$request->get('role'));
        
        // In case our POST was a success we need to return a 201 HTTP CREATED response with the created object
        return View::create($user, Response::HTTP_CREATED);
    }
    /**
     * Creates new account
     * @Rest\Post("/login.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function login(Request $request): View
    {
        $user = $this->userService->login($request->get('username'), $request->get('password'));
        // In case our POST was a success we need to return a 201 HTTP CREATED response with the created object
        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Replaces User  resource
     * @Rest\Put("/account/changeCredientials.{_format}", defaults={"_format"="json"})
     * @param int $userId
     * @param Request $request
     * @return View
     */
    public function changeCredentials(Request $request): View
    {
        $user = $this->userService->updateUser($this->userId, $request->get('newUsername'), $request->get('newPassword'),'ROLE_USER');
        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($user, Response::HTTP_OK);
    }

    /**
     * Removes the User resource
     * @Rest\Delete("/account/delete.{_format}", defaults={"_format"="json"})
     * @param int $userId
     * @return View
     */
    public function deleteAccount(): View
    {
        $this->userService->deleteUser($this->userId);
        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }
}