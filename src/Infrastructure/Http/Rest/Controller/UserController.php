<?php

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class UserController
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class UserController extends FOSRestController
{
    /**
     * @var UserService
     */
    private $userService;

    

    private $request;
    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Creates an User resource
     * @Rest\Post("/users.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function postUser(Request $request): View
    {
        $user = $this->userService->addUser($request->get('username'), $request->get('name'), $request->get('surname'), $request->get('password'), $request->get('role'));
        
        // In case our POST was a success we need to return a 201 HTTP CREATED response with the created object
        //exclude password for response
        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Retrieves an User resource
     * @Rest\Get("/users/{userId}.{_format}", defaults={"_format"="json"})
     * @param int $userId
     * @return View
     */
    public function getUserObject(int $userId): View
    {
        $user = $this->userService->getUser($userId);


        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($user, Response::HTTP_CREATED);        
    }

    /**
     * Retrieves a collection of User resource
     * @Rest\Get("/users.{_format}", defaults={"_format"="json"})
     * @return View
     */
    public function getUsers(): View
    {
        $users = $this->userService->getAllUsers();

        // In case our GET was a success we need to return a 200 HTTP OK response with the collection of user object
        return View::create($users, Response::HTTP_CREATED);
        
    }

    /**
     * Replaces User resource
     * @Rest\Put("/users/{userId}.{_format}", defaults={"_format"="json"})
     * @param int $userId
     * @param Request $request
     * @return View
     */
    public function putUser(int $userId, Request $request): View
    {
        $user = $this->userService->updateUser($userId, $request->get('username'), $request->get('name'), $request->get('surname'), $request->get('password'),$request->get('role'));

        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Removes the User resource
     * @Rest\Delete("/users/{userId}.{_format}", defaults={"_format"="json"})
     * @param int $userId
     * @return View
     */
    public function deleteUser(int $userId): View
    {
        $this->userService->deleteUser($userId);

        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }
}